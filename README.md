## Burning Bridges ##

In this game you are randomily given a position on a grid and have a finite
amount of time to move off your current position. In what ever dierction you
move you burn the bridge you crossed over and can not take it again. Your
goal is to clear out as many complete squares as you can without getting caught.

Random bridges might be burned ahead of time, and if you do not decide where
to move then your bridge burns and you lose.

Good luck!

Name: Matthew Williamson
Email: mwilli20@binghamton.edu

Name: John Weachock
Email: jweacho1@binghamton.edu