package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;

public class GameView extends View {
    private Timer mTimer = new Timer();
    final static int RADIUS = 30;
    final static float LINE_WIDTH = 15;
    final static int TURN_LENGTH = 450;
    final static String TITLE = "Burning Bridges";
    final static String GAME_OVER = "Game Over";

    final static int STATE_PLAY = 0;
    final static int STATE_DEAD = 1;

    final static int[][] dir2vec = new int[][]{
            new int[]{-1, 0},  // 0
            new int[]{-1, -1}, // 1
            new int[]{0, -1},  // 2
            new int[]{1, -1},  // 3
            new int[]{1, 0},   // 4
            new int[]{1, 1},   // 5
            new int[]{0, 1},   // 6
            new int[]{-1, 1}}; // 7

    private Random rand;
    private int size, x, y, dx, dy, dir, score, highScore;
    private int x_offset, y_offset, spacing;
    private double startX, startY, endX, endY, tempX, tempY;
    private boolean touching;
    private boolean bridges[][][];
    private int state;

    Context context;
    int ticks;
    FontSprite banner;
    FontSprite scoreBanner;
    FontSprite highScoreBanner;
    ProgressWheel wheel;
    Paint circlePaint;
    Paint linePaint;
    Paint tempPaint;
    ResourceManager res;

    /**
     * Probably not self explanatory, but this is how the
     * boolean[8] is set up
     * <p/>
     * 1  2  3
     * 0  x  4
     * 7  6  5
     */
    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.context = context;
        this.res = new ResourceManager(context);
        this.scoreBanner = new FontSprite(this.context, "0");
        this.highScoreBanner = new FontSprite(this.context, "0");
        this.banner = new FontSprite(this.context, TITLE);
        this.wheel = new ProgressWheel(this.context, TURN_LENGTH);
        this.circlePaint = new Paint();
        this.circlePaint.setColor(Color.RED);

        this.linePaint = new Paint();
        this.linePaint.setColor(Color.BLACK);
        this.linePaint.setStrokeWidth(LINE_WIDTH);
        this.linePaint.setStrokeCap(Paint.Cap.ROUND);

        this.tempPaint = new Paint(this.linePaint);
        this.tempPaint.setColor(Color.BLUE);

        this.rand = new Random();
        this.size = 6;
        this.bridges = new boolean[this.size][this.size][8];
        reset();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                GameView.this.tick();
                GameView.this.postInvalidate();
            }
        }, 0, 16);
    }

    void tick() {
        if (this.state == STATE_PLAY) {
            this.ticks++;
            this.wheel.update(this.ticks);

            if (this.ticks == TURN_LENGTH) {
                end_game();
            }
        }
    }

    private void end_game() {
        this.banner.text(GAME_OVER);
        this.state = STATE_DEAD;
        this.wheel.update(TURN_LENGTH);
    }

    private int getDir(double x, double y) {
        int angle = (int) (Math.toDegrees(Math.atan2(y, x)) + 360) % 360;
        int dir = 4;
        if (22 < angle && angle <= 67) {
            dir = 5;
        } else if (67 < angle && angle <= 112) {
            dir = 6;
        } else if (112 < angle && angle <= 157) {
            dir = 7;
        } else if (157 < angle && angle <= 202) {
            dir = 0;
        } else if (202 < angle && angle <= 247) {
            dir = 1;
        } else if (247 < angle && angle <= 292) {
            dir = 2;
        } else if (292 < angle && angle <= 337) {
            dir = 3;
        }
        return dir;
    }

    private void makeMove() {
        double x = this.endX - this.startX;
        double y = this.endY - this.startY;
        this.dir = this.getDir(x, y);
        this.dx = dir2vec[this.dir][0];
        this.dy = dir2vec[this.dir][1];

        boolean good = move();
        if (!good) {
            end_game();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (this.state == STATE_PLAY) {
            if (action == MotionEvent.ACTION_DOWN) {
                this.startX = event.getX();
                this.startY = event.getY();
                this.touching = true;
            } else if (action == MotionEvent.ACTION_MOVE) {
                this.tempX = event.getX();
                this.tempY = event.getY();
            } else if (action == MotionEvent.ACTION_UP) {
                this.endX = event.getX();
                this.endY = event.getY();
                this.touching = false;
                this.makeMove();
            }
        } else if (this.state == STATE_DEAD) {
            if (action == MotionEvent.ACTION_UP) {
                this.reset();
            }
        }
        return true;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.x_offset = w / 10;
        this.y_offset = this.x_offset * 3;
        this.spacing = (this.getWidth() * 4 / 5) / (this.size - 1);

        this.banner.move(w / 2, w / 5);
        this.wheel.move(w / 2, 3 * x_offset + spacing * this.size);
        this.scoreBanner.move(w / 2, 5 * x_offset + spacing * this.size);
        this.highScoreBanner.move(w / 2, 7 * x_offset + spacing * this.size);
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.scoreBanner.draw(canvas);
        this.highScoreBanner.draw(canvas);
        this.banner.draw(canvas);
        this.wheel.draw(canvas);

        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                for (int k = 0; k < 8; k++) {
                    if (this.bridges[i][j][k]) {
                        int di = dir2vec[k][0];
                        int dj = dir2vec[k][1];
                        canvas.drawLine(i * spacing + x_offset, j * spacing + y_offset,
                                (i + di) * spacing + x_offset, (j + dj) * spacing + y_offset,
                                this.linePaint);
                    }
                }
            }
        }
        canvas.drawCircle(this.x * spacing + x_offset, this.y * spacing + y_offset,
                RADIUS, this.circlePaint);

        if (this.touching) {
            int tempDir = this.getDir(this.tempX - this.startX, this.tempY - this.startY);
            float dx = dir2vec[tempDir][0];
            float dy = dir2vec[tempDir][1];
            canvas.drawLine(this.x * spacing + x_offset, this.y * spacing + y_offset,
                    (this.x + dx) * spacing + x_offset, (this.y + dy) * spacing + y_offset,
                    this.tempPaint);
        }
    }

    public void reset() {
        this.banner.text(TITLE);
        this.x = rand.nextInt(this.size - 1);
        this.y = rand.nextInt(this.size - 1);
        this.score = 0;
        this.scoreBanner.text("" + 0);
        this.ticks = 0;
        this.touching = false;
        this.state = STATE_PLAY;
        this.wheel.update(this.ticks);

        int s = this.size - 1;
        resetHelper(0, 0, new int[]{0, 1, 2, 3, 7}, new int[]{4, 5, 6});
        for (int j = 1; j < s; j++) {
            resetHelper(j, 0, new int[]{1, 2, 3}, new int[]{0, 4, 5, 6, 7});
        }
        resetHelper(s, 0, new int[]{1, 2, 3, 4, 5}, new int[]{0, 6, 7});

        for (int i = 1; i < s; i++) {
            resetHelper(0, i, new int[]{0, 1, 7}, new int[]{2, 3, 4, 5, 6});
            for (int j = 1; j < s; j++) {
                resetHelper(j, i, new int[]{}, new int[]{0, 1, 2, 3, 4, 5, 6, 7});
            }
            resetHelper(s, i, new int[]{3, 4, 5}, new int[]{0, 1, 2, 6, 7});
        }

        resetHelper(0, s, new int[]{0, 1, 5, 6, 7}, new int[]{2, 3, 4});
        for (int j = 1; j < s; j++) {
            resetHelper(j, s, new int[]{5, 6, 7}, new int[]{0, 1, 2, 3, 4});
        }
        resetHelper(s, s, new int[]{3, 4, 5, 6, 7}, new int[]{0, 1, 2});
    }

    private void resetHelper(int i, int j, int[] outer, int[] inner) {
        for (int k : outer) {
            this.bridges[i][j][k] = false;
        }
        for (int k : inner) {
            boolean r = this.rand.nextInt(14) > 1;
            int di = dir2vec[k][0];
            int dj = dir2vec[k][1];
            this.bridges[i][j][k] = r;
            this.bridges[i + di][j + dj][anti(k)] = r;
        }
    }

    public boolean move() {
        // Check if choice is open
        if (!this.bridges[this.x][this.y][this.dir]) {
            return true;
        }
        this.ticks = 0;
        // Update position
        this.bridges[this.x][this.y][this.dir] = false;
        this.x += this.dx;
        this.y += this.dy;
        this.bridges[this.x][this.y][anti(this.dir)] = false;
        // Update score
        this.score += 1;
        if (this.highScore < this.score) {
            this.highScore = this.score;
        }
        this.scoreBanner.text("" + this.score);
        this.highScoreBanner.text("" + this.highScore);
        // Check if point is dead
        boolean isDead = false;
        for (boolean b : this.bridges[this.x][this.y]) {
            isDead |= b;
        }
        return isDead;
    }

    private int anti(int n) {
        return (n + 4) % 8;
    }
}
